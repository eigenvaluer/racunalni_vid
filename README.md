## UPUTE

1. Kloniraj cijeli repo (git clone https://eigenvaluer@bitbucket.org/eigenvaluer/racunalni_vid.git)
2. (OPCIONALNO) U racunalni_vid/data postavi ulazni video. Vec postoje video sekvence pa ovaj korak možeš preskočiti.
3. Pozicioniraj se u: racunalni_vid\Least squares (Lukas Kanade) Python\LukasKanadeHarris\ 
4. Pozovi naredbu: python LukasKanadeHarris.py -video-in running_3s.mp4 -images-out imout -video-out vidout -window-size 40 -smooth-window-size 3 -derivative-mask-size 3 -lower-res 2 -vector-scale 0.1
5. Izlaz se nalazi u istom direktoriju u kojem si pozicioniran. Video datoteka izlaza je "vidout". Slike koje su medjurezultat su u "imout"
6. Objašnjenja parametara:  

	6.1  -video-in crowd.mp4 -- ime ulazne video sekvence koja mora biti u racunalni_vid/data  
	
	6.2  -images-out imout -- ime direktorija gdje su spremljene slike iz medjurezultata. Taj direktorij se nalazi u direktoriju u kojem si pozicioniran  
	
	6.3  -video-out vidout -- ime datoteke gdje je izlazni video. Ta datoteka se nalazi u direktoriju u kojem si pozicioniran  
	
	6.4  -window-size 40 -smooth-window-size 3 -derivative-mask-size 3  -- parametri algoritma OF. Vidi u help.  
	
	6.5. -lower-res 2 -- koliko puta algoritam smanji rezoluciju ulaznog videa pri procesiranju.  
	
	6.6. -vector-scale 0.1 -- skala vektora koje dobivamo u izlaznom videu
	

## STARTING OUT

**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).