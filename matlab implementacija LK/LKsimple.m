close all;
clearvars;
clc;

% READ VIDEO FILE AS ITERATOR
video = VideoReader('C:\Users\Lovro\Documents\FER\CV\Projekt\Gradijenti pristup\Implementacija\racunalni_vid\data\simple.mp4');

% GET FIRST FRAME
frame1 = readFrame(video);
disp(size(frame1))

% WINDOW FOR DISPLAYING THE VIDEO
figure();

% FOR EVERY FRAME
frame_cnt = 0;

while hasFrame(video)
    
    % TAKE NEXT FRAME
    frame2 = readFrame(video);
    
    if mod(frame_cnt, 10) == 0

        % CALCULATE OPTICAL FLOW FOR PREVIOUS AND NEXT FRAME
        [u,v,X,Y] = LK(45, frame1, frame2);

        % draw vectors
        imshow(frame2);
        hold on
        quiver(X, Y, u, v, 'y')

        % swap frames
        frame1 = frame2;

    end
    
    frame_cnt = frame_cnt + 1;
    
    
end

%% Function for optical flow calculation (Lukas kanade)
function [u,v,X,Y] = LK(ww, frame1_org, frame2_org)

    % to grayscale and double precision
    frame1 = im2double(rgb2gray(frame1_org));
    frame2 = im2double(rgb2gray(frame2_org));
    
    % lower resolution
    frame1_smaller = imresize(frame1, 0.25);
    frame2_smaller = imresize(frame2, 0.25);
    disp(size(frame1_smaller));

    % window around specific pixel for optical flow calculation
    w = round(ww/2);
    
    % derivation masks
    der_x_mask = [-1 1; -1 1];
    der_y_mask = [-1 -1; 1 1];
    der_1_mask = ones(2);
    der_2_mask = -ones(2);
    
    % derivatives of frame1 and time difference between frame1 and  frame2
    Ix_m = conv2(frame1_smaller, der_x_mask, 'valid');
    Iy_m = conv2(frame1_smaller, der_y_mask, 'valid');
    It_m = conv2(frame1_smaller, der_1_mask, 'valid') + conv2(frame2_smaller, der_2_mask, 'valid');
    
    % flow vectors
    u = zeros(size(frame1_smaller));
    v = zeros(size(frame2_smaller));
    
    % grid for optical flow calculation
    disp(length(Ix_m))
    [n, m] = size(Ix_m); % m=640, n=360
    [X, Y] = meshgrid(1:n, 1:m);
    X = X(w+1:20:end-w, w+1:20:end-w); % mind the window not crossing image borders
    Y = Y(w+1:20:end-w, w+1:20:end-w);
    xy = [X(:), Y(:)];
    
    % for every point on grid
    for k = 1:length(xy)
        
        pos = xy(k,:);
        i = pos(1);
        j = pos(2);

        Ix = Ix_m(i-w:i+w, j-w:j+w);
        Iy = Iy_m(i-w:i+w, j-w:j+w);
        It = It_m(i-w:i+w, j-w:j+w);

        Ix = Ix(:);
        Iy = Iy(:);
        b = - It(:);

        A = [Ix, Iy];
        nu = pinv(A) * b;

        u(i,j) = nu(1);
        v(i,j) = nu(2);
        
    end
    u = u';
    v = v';
    [X, Y] = meshgrid(1:n+1, 1:m+1);
     
end