# -*- coding: utf-8 -*-
"""
Created on Thu Nov  1 11:39:25 2018

@author: Lovro
"""

import numpy as np
import cv2 as cv

from matplotlib import pyplot as plt
from IPython import get_ipython
get_ipython().run_line_magic('matplotlib', 'qt')

from algorithm import two_frame_OF

"*************************** HELPER FUNCTIONS ********************************"

"lowers resolution and converts image to grayscale"
def low_res_to_gray(image, times=0):
    
    img_p = image.copy()
    
    img_p = cv.cvtColor(img_p, cv.COLOR_BGR2GRAY)

    for i in range(times):
        img_p = cv.pyrDown(img_p)
        
    return img_p

"""
Display optical flow for two frames.
f1, f2 - two frames for which OF is calculated
xy - grid points on frame where OF is calculated
flow_vectors - magnitudes of flow vector in u and v direction calculated in points xy
"""
def plot_OF_one_image(f1, f2, xy, flow_vectors):
    
    " ini matrices that will contain contain flow vectors magnitude for u and v direction"
    flow_matrix_U = np.zeros(shape=(f1.shape[0],f1.shape[1]))
    flow_matrix_V = np.zeros(shape=(f1.shape[0],f1.shape[1]))
    
    "ini matrix that will contain grid points where OF was calculated"
    points = np.zeros(shape=(f1.shape[0],f1.shape[1]))

    "for every grid point"
    idx = 0
    for pos in xy:
    
        "take i and j coordinates"
        i = pos[0]
        j = pos[1]
    
        "fill matrices with magnitudes of flow vectors"
        flow_matrix_U[i,j] = flow_vectors[idx][0] # okreni?
        flow_matrix_V[i,j] = flow_vectors[idx][1]
        
        "fill matrix with grid points where OF was caluclated"
        points = cv.circle(points,(j,i),10,(100,100,0),-1)
        
        "number of grid points is equal to number of flow vectors"
        idx += 1
        
    print(flow_matrix_U.shape)
    print(flow_matrix_V.shape)
        
    "create meshgrid for plotting"
    x = list((range(0, f1.shape[0]))) # number of raws is y
    y = list((range(0, f1.shape[1])))
    X, Y = np.meshgrid(x, y, indexing='ij')
    
    "plot image, points where OF was calculated and flow vectors"
    
    "replace old figure"
    plt.figure(1, figsize=(40,20)); plt.clf()
    
    "display grid, f1, f2"
    img = points+f2+f1
    plt.imshow(img)
    
    "display optical flow"
    cmap = plt.get_cmap('Reds')
    plt.quiver(Y, X, flow_matrix_V, flow_matrix_U, cmap=cmap, scale=1, units="xy")
    
    "wait for 0.5s"
    plt.pause(0.1)
    
    
"*************************** TEST FUNCTIONS **********************************"

def optical_flow_for_video():
    
    "file name of video"
    video_file = '..' + '\\' + 'data'+'\\'+'crowd-1_mp4.mp4'
    
    "read a video using opencv"
    video = cv.VideoCapture(video_file)
    
    "number of frames"
    no_of_frames = int(video.get(cv.CAP_PROP_FRAME_COUNT))
    print(no_of_frames)
    
    "first frame"
    status_1, frame_1 = video.read()
    f1 = low_res_to_gray(frame_1, times=2)
    
    "iterate through video frames"
    for frame_no in range(1, no_of_frames+1):
        
        print("FRAME", frame_no)
        
        if (frame_no % 3 != 0):
            video.read()
            continue
        
        "read next frame"
        status_2, frame_2 = video.read()
        f2 = low_res_to_gray(frame_2, times=2)
    
        "if last frame, break"
        if not status_2:
            break
        
        flow_vectors, xy = two_frame_OF(f1, f2, window_size=5, n_grid_points=10)
    
        plot_OF_one_image(f1, f2, xy, flow_vectors)
        
        f1 = f2
        
        
    "close video file stream"
    video.release()
        
        
def optical_flow_for_two_images():
    
    f1 = cv.imread('..' + '\\' + 'data'+'\\'+ '1.png')
    f2 = cv.imread('..' + '\\' + 'data'+'\\'+ '3.png')
    
    f1 = low_res_to_gray(f1, times=2)
    f2 = low_res_to_gray(f2, times=2)
    
    plt.figure(1, figsize=(40,20))
    plt.imshow(f1)
    plt.show()
    
    plt.figure(2, figsize=(40,20))
    plt.imshow(f2)
    plt.show()
    
    flow_vectors, xy = two_frame_OF(f1, f2, window_size=5, n_grid_points=10)
    
    plot_OF_one_image(f1, f2, xy, flow_vectors)
    
    

"*************************** MAIN FUNCTION ***********************************"

def main():
    
    optical_flow_for_two_images()
    #optical_flow_for_video()
    

     
"RUN"
if __name__ == "__main__":
    main()
    
    
    
    
