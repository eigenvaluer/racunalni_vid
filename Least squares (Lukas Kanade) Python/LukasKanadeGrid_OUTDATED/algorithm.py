# -*- coding: utf-8 -*-
"""
Created on Sat Dec  8 15:53:52 2018

@author: Lovro
"""
import numpy as np
import cv2 as cv
from sklearn.preprocessing import normalize

from matplotlib import pyplot as plt
from IPython import get_ipython
get_ipython().run_line_magic('matplotlib', 'qt')

import scipy.signal as si

"*************************** OPTICAL FLOW ************************************"

"""
Least squares optical flow implementation - Lukas Kanade algorithm.
Given two gray images (two frames from video) with desiered resolution, calculates OF
"""
def two_frame_OF(frame_1, frame_2, window_size=10, n_grid_points=20):
    
    "dimension of a frame"
    frame_shape = frame_1.shape
    
    "smooth images with gaussian"
    smooth_window=(3,3) #neparna maska!
    frame_1_smooth = cv.GaussianBlur(frame_1, smooth_window, 0)
    frame_2_smooth = cv.GaussianBlur(frame_2, smooth_window, 0)
    
    "calculate x, y and t derivatives"
    frame_1_x_derivative = cv.Sobel(frame_1_smooth, cv.CV_64F, 1, 0, ksize=1) #neparna maska!
    frame_1_y_derivative = cv.Sobel(frame_1_smooth, cv.CV_64F, 0, 1, ksize=1)
    
    frames_t_derivative = frame_2 - frame_1
    
    "This list will contain magnitures of flow vectors in u and v direction"
    motion_vectors = []
    
    "create meshgrid of pixel position for optical flow calculation"
    x = range(window_size, frame_shape[0]-window_size, n_grid_points)
    y = range(window_size, frame_shape[1]-window_size, n_grid_points)
    xv, yv = np.meshgrid(x, y)
    xy = np.array(list(zip(xv.ravel(), yv.ravel())))
    
    "for every position calculate OF"
    for pos in xy:
        
        i = pos[0]
        j = pos[1]
           
        "take pixels around grid point"
        Ix = frame_1_x_derivative[i-window_size:i+window_size+1, j-window_size:j+window_size+1].flatten()
        Iy = frame_1_y_derivative[i-window_size:i+window_size+1, j-window_size:j+window_size+1].flatten()
        It = frames_t_derivative[i-window_size:i+window_size+1, j-window_size:j+window_size+1].flatten()
        
        
        "M matrix"
        M = np.column_stack([Ix, Iy])
        
        "b vector"
        b = -It
        
        "B VERSION M matrix"
        #M = np.array([[np.sum(np.power(Ix,2)), np.sum(Ix*Iy)], [np.sum(Ix*Iy), np.sum(np.power(Iy,2))]])
        
        "B VERSION b vector"
        #b = -np.array([[np.sum(Ix*It)],[np.sum(Iy*It)]])
     
        
        "for motion vecotrs, calculate pseudoinverse"
        #motion_vector = np.dot(np.dot(np.linalg.pinv(np.dot(M.T,M)), M.T), b) 
        motion_vector = np.dot(np.linalg.pinv(M), b)
        motion_vectors.append(motion_vector)
        
        print(i,j,motion_vector)
    
    return motion_vectors, xy


"""
"If eigenvalues of M.T * M too small do not calculate pseudoinverse"
        eigenvalues, eigenvectors = np.linalg.eig(np.dot(M, np.transpose(M)))
        min_eigenval = np.min(eigenvalues)
        print(min_eigenval)
        if 0.01 > min_eigenval:
            motion_vectors.append((np.array([0, 0])))
            continue
"""

