# -*- coding: utf-8 -*-
"""
Created on Thu Nov  1 11:44:14 2018

@author: Lovro
"""

import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt
from IPython import get_ipython
get_ipython().run_line_magic('matplotlib', 'qt')

"lowers resolution and converts image to grayscale"
def low_res_to_gray(image, times=0):
    
    img_p = image.copy()
    
    img_p = cv.cvtColor(img_p, cv.COLOR_BGR2GRAY)

    for i in range(times):
        img_p = cv.pyrDown(img_p)
        
    return img_p

"""
TEST: video frame difference
"""
def test_im_diff():
    
    "file name of video"
    video_file = '..' + '\\' + 'data'+'\\'+'crowd-1_mp4.mp4'
    
    "read a video using opencv"
    video = cv.VideoCapture(video_file)
    
    "number of frames"
    no_of_frames = int(video.get(cv.CAP_PROP_FRAME_COUNT))
    
    "first frame"
    status_1, frame_1 = video.read()
    f1 = low_res_to_gray(frame_1, times=2)
    
    "iterate through video frames"
    for frame_no in range(1, no_of_frames+1):
        
        if (frame_no % 3 != 0):
            video.read()
            continue
        
        "read next frame"
        status_2, frame_2 = video.read()
        f2 = low_res_to_gray(frame_2, times=2)
    
        "if last frame, break"
        if not status_2:
            break
        
        "smooth images with gaussian"
        smooth_window=(25,25) #neparna maska!
        frame_1_smooth = cv.GaussianBlur(f1, smooth_window, 0)
        frame_2_smooth = cv.GaussianBlur(f2, smooth_window, 0)
        
        "replace old figure"
        plt.figure(1, figsize=(40,20)); plt.clf()
        
        
        img = np.abs(frame_1_smooth-frame_2_smooth)
        
        "connected components TODO"
        #nlabels, labelsMap, stats, centroids = cv.connectedComponentsWithStats(img)
        #area = stats[:,4]
        #area[0] = 0
        #max_label = np.argmax(area)
        #img2 = np.zeros(labelsMap.shape)
        
        "display grid, f1, f2"
        plt.imshow(img)
        
        "wait for 0.5s"
        plt.pause(0.1)
        
        f1 = f2
        
        
test_im_diff()
        

"""     
TEST: harris corner detection on video
"""
def harris_video():
    
    "file name of video"
    video_file = '..' + '\\' + 'data'+'\\'+'crowd-1_mp4.mp4'
    
    "read a video using opencv"
    video = cv.VideoCapture(video_file)
    
    "number of frames"
    no_of_frames = int(video.get(cv.CAP_PROP_FRAME_COUNT))
    
    "first frame"
    status_1, frame_1 = video.read()
    f1 = low_res_to_gray(frame_1, times=2)
    
    "iterate through video frames"
    for frame_no in range(1, no_of_frames+1):
        
        #if (frame_no % 1 != 0):
            #video.read()
            #continue
        
        "read next frame"
        status_2, frame_2 = video.read()
        f2 = low_res_to_gray(frame_2, times=2)
    
        "if last frame, break"
        if not status_2:
            break
        
        "replace old figure"
        plt.figure(1, figsize=(40,20)); plt.clf()
        
        "display grid, f1, f2"
        hc = cv.cornerHarris(f1, 2, 3, 0.04)
        print("KUTOVI", np.argwhere(hc>0.01*hc.max()))
        f1[hc>0.01*hc.max()]=[255]
        plt.imshow(f1)
        
        "wait for 0.5s"
        plt.pause(0.1)
        
        f1 = f2
    
#harris_video()   

"""
A = np.array([[3, 4], [4,2]])
print(A / np.linalg.norm(A))
val, vec = np.linalg.eig(A)
print(val)
b = np.array([1,2])
c = np.dot(np.linalg.pinv(A), b )
print(c)
"""

"""
nx, ny = (5,4)
x = range(0, nx, 2)
y = range(0, ny, 2)
xv, yv = np.meshgrid(x, y)
xy = np.array(list(zip(xv.ravel(), yv.ravel())))
for i in xy:
    print(i)
"""


"""
img = cv.imread('..'+'\\'+'data'+'\\'+'box.jpg')
img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
corners = cv.goodFeaturesToTrack(img, 25, 0.01, 10)
print(corners[0][0][0])
for i in corners:
    print(i[0][0])
"""

"""
video = cv.VideoCapture("simple.mp4")
status_1, frame_1 = video.read()
frame_1 = cv.cvtColor(frame_1, cv.COLOR_BGR2GRAY)
video.set(cv.CAP_PROP_POS_FRAMES,50)
status_2, frame_2 = video.read()
frame_2 = cv.cvtColor(frame_2, cv.COLOR_BGR2GRAY)

frame1_p = frame_1.copy()
frame2_p = frame_2.copy()
for i in range(4):
    frame1_p = cv.pyrDown(frame1_p)
    frame2_p = cv.pyrDown(frame2_p)

cv.namedWindow('5', cv.WINDOW_NORMAL)
cv.resizeWindow('5', 640, 480)
cv.imshow('5', frame1_p)
cv.waitKey(0)

cv.namedWindow('6', cv.WINDOW_NORMAL)
cv.resizeWindow('6', 640, 480)
cv.imshow('6', frame2_p)
cv.waitKey(0)

flow_vectors = (two_frame_OF(frame1_p, frame2_p))

#sobelx = cv.Sobel(img, cv.CV_64F, 1, 0, ksize=5)
#sobely = cv.Sobel(img, cv.CV_64F, 0, 1, ksize=5)

flow = np.zeros_like(frame1_p)

for flow_vector in flow_vectors:
            
            flow = cv.line(flow, 
                           (int(flow_vector[0][0]), int(flow_vector[0][1])), 
                           (int(flow_vector[1][0]), int(flow_vector[1][1])),
                           (255,0,0), 1)
            
            
flow_img = cv.add(frame1_p, flow)
cv.namedWindow('6', cv.WINDOW_NORMAL)
cv.resizeWindow('6', 640, 480)
cv.imshow('6', flow_img)
cv.waitKey(0)

cv.destroyAllWindows()
"""