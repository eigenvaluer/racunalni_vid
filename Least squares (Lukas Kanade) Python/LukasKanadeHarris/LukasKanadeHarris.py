# -*- coding: utf-8 -*-
"""
Created on Tue Dec 18 14:54:40 2018

@author: Lovro
"""


"************************** IMPORTS ******************************************"

########################### STANDARD ##########################################
import matplotlib
import argparse
import os

######################### USER DEFINED ########################################
from common import optical_flow_for_video

"************************ MAIN ***********************************************"
def main():
    
    "unables plotting to screen"
    matplotlib.use('Agg') 
    
    parser = argparse.ArgumentParser()
    
    parser.add_argument("-video-in",
                        help="Name of video file located in data folder", 
                        type=str)
    
    parser.add_argument("-images-out",
                        help="Name of directory where output images will be stored",
                        type=str)
    
    parser.add_argument("-video-out",
                        help="Name of file where output video will be stored",
                        type=str)
    
    parser.add_argument("-window-size",
                        help="Size of widow around point where OF is calculated.",
                        type=int)
    
    parser.add_argument("-smooth-window-size",
                        help="Size of widow for Gauss smoothing. ODD NUMBERS ONLY!",
                        type=int)
    
    parser.add_argument("-derivative-mask-size",
                        help="Size of widow for Sobel derivative. ODD NUMBERS ONLY!",
                        type=int)
    
    parser.add_argument("-lower-res",
                        help="Number of times to lower resolution of video during processing",
                        type=int)
    
    parser.add_argument("-vector-scale",
                        help="Scale of OF vectors in resulting video",
                        type=float)
    
    args = parser.parse_args()
    
    "file name of video"
    video_file = os.path.join('..', '..', 'data', args.video_in)
    
    "directory for images"
    images_folder = os.path.join(args.images_out)
    
    "name of video"
    video_out = os.path.join(args.video_out)
    
    "optical flow, LukasKanade algorithm params"
    params = {"window_size":args.window_size, 
              "smooth_window_size":(args.smooth_window_size, args.smooth_window_size),
              "derivative_mask_size":args.derivative_mask_size,
              "lower_res":args.lower_res,
              "vector_scale":args.vector_scale}
    
    print("Ulazni video:", video_file)
    print("Izlazne slike:", images_folder)
    print("Izlazni video", video_out)
    print("ZADANI PARAMETRI:")
    print(params)
    
    "calculate optical flow for every seqeuent pair of images in video"
    optical_flow_for_video(video_file, 
                           images_folder, 
                           params,
                           video_out)
    
    print("GOTOVO!")
    print("Video datoteka se nalazi u direktoriju u kojem je i ovaj program")
    print("Izlazne slike se nalaze u direktoriju u kojem je i ovaj program")

"************************ RUN ***********************************************"
if __name__  == "__main__":
    main()


