# -*- coding: utf-8 -*-
"""
Created on Thu Jan  3 13:56:15 2019

@author: Lovro
"""

"************************** IMPORTS ******************************************"

########################### STANDARD ##########################################
import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt
import os

######################### USER DEFINED ########################################
from TwoFrameOF import two_frame_OF

"************************ FUNCTIONS ******************************************"


"""
"lowers resolution and converts image to grayscale"
"""
def low_res_to_gray(image, times=0):
    
    img_p = image.copy()
    
    img_p = cv.cvtColor(img_p, cv.COLOR_BGR2GRAY)

    for i in range(times):
        img_p = cv.pyrDown(img_p)
        
    return img_p

"""
"plotting optical flow on image and saving images to folder"
"""
def plot_OF_one_image(f1, f2, xy, flow_vectors, cnt, dest_file, vector_scale):
    
    " ini matrices that will contain contain flow vectors magnitude for u and v direction"
    flow_matrix_U = np.zeros(shape=(f1.shape[0],f1.shape[1]))
    flow_matrix_V = np.zeros(shape=(f1.shape[0],f1.shape[1]))
    
    "ini matrix that will contain grid points where OF was calculated"
    points = np.zeros(shape=(f1.shape[0],f1.shape[1]))

    "for every grid point"
    idx = 0
    for pos in xy:
    
        "take i and j coordinates"
        i = pos[0]
        j = pos[1]
    
        "fill matrices with magnitudes of flow vectors"
        flow_matrix_U[i,j] = flow_vectors[idx][0]
        flow_matrix_V[i,j] = flow_vectors[idx][1]
        
        "fill matrix with grid points where OF was caluclated"
        points = cv.circle(points,(j,i),40,(100,100,0),-1)
        
        "number of grid points is equal to number of flow vectors"
        idx += 1
        
    "create meshgrid for plotting"
    x = list((range(0, f1.shape[0]))) 
    y = list((range(0, f1.shape[1])))
    X, Y = np.meshgrid(x, y, indexing='ij')
    
    "replace old figure"
    plt.figure(1, figsize=(40,20)); plt.clf()
    
    "plot frame and points where OF is calculated"
    img = f1 #+ points
    plt.imshow(img)
    
    "display optical flow vectors on frame"
    cmap = plt.get_cmap('Reds')
    plt.quiver(Y, X, flow_matrix_V, flow_matrix_U, cmap=cmap, scale=vector_scale, units="xy")
    
    "save image"
    save_name = os.path.join(dest_file, str(cnt))
    plt.savefig(save_name)
    
"""
"run optical flow for whole video"
"""
def optical_flow_for_video(video_file, dest_folder, params, video_out):
    
    "create resulting folder"
    if not os.path.exists("Rezultati"):
        os.mkdir("Rezultati")
        
    dest_folder = os.path.join("Rezultati", dest_folder)
    "create destination folder if it is not existing"
    if not os.path.exists(dest_folder):
        os.mkdir(dest_folder)
    
    "read a video using opencv"
    video = cv.VideoCapture(video_file)
    
    "number of frames"
    no_of_frames = int(video.get(cv.CAP_PROP_FRAME_COUNT))
    print("Number of frames", no_of_frames)
    
    "take first frame, lower resolution and convert to grayscale"
    status_1, frame_1 = video.read()
    f1 = low_res_to_gray(frame_1, times=params["lower_res"])
    
    "iterate through rest of video frames"
    cnt = 0
    for frame_no in range(1, no_of_frames+1):
        
        cnt += 1
        print(cnt,"/",no_of_frames)
        
        "take only specific frames"
        if (frame_no % 1 != 0):
            video.read()
            print("FRAME", frame_no)
            continue
        
        "read next frame, lower resolution and convert to grayscale"
        status_2, frame_2 = video.read()
        
        "if last frame, break"
        if not status_2:
            break
        
        f2 = low_res_to_gray(frame_2, times=params["lower_res"])
    
        "for two current frames calculate OF"
        flow_vectors, xy = two_frame_OF(f1, 
                                        f2, 
                                        window_size=params["window_size"], 
                                        smooth_window_size=params["smooth_window_size"], 
                                        derivative_mask_size=params["derivative_mask_size"])
    
        "plot optical flow on image and save image"
        plot_OF_one_image(f1, f2, xy, flow_vectors, cnt, dest_folder, params["vector_scale"])
        
        "new frame becomes old"
        f1 = f2
        
    "close video file stream"
    video.release()
    
    "from images generated in optical_flow_for_video create video"
    images_to_video(dest_folder, video_out)
    
"""
"read images from file and create video"
"""
def images_to_video(images_file, video_out):
    
    "take all image names from folder"
    images = os.listdir(images_file)
    
    "if no images finish"
    if len(images) == 0:
        return
    
    "find shape of out image"
    src = os.path.join(images_file, images[0])
    img = cv.imread(src)
    img_shape = img.shape
    
    "create video writer"
    PATH = os.path.join("Rezultati", video_out+".avi")
    out = cv.VideoWriter(PATH, cv.VideoWriter_fourcc('M','J','P','G'), 10, (img_shape[1], img_shape[0]))
    
    "sort images by numbers"
    numbers = []
    for image in images:
        numbers.append(int(image.split(".")[0]))
        
    image_num = zip(images, numbers)
    image_num_s = sorted(image_num, key = lambda t: t[1])
    images, numbers = zip(*image_num_s)
    
    "take sored images and save to video"
    for image in images:
        print("SPREMAM", image)
        src = os.path.join(images_file, image)
        img = cv.imread(src)
        img = cv.cvtColor(img, cv.COLOR_BGR2RGB)
        out.write(img)
        
    out.release()
        