# -*- coding: utf-8 -*-
"""
Created on Thu Jan  3 13:57:04 2019

@author: Lovro
"""


"************************** IMPORTS ******************************************"

########################### STANDARD ##########################################
import cv2 as cv
import numpy as np

######################### USER DEFINED ########################################


"**************************** ALGORITHM **************************************"

"optical flow calculation for two frames"
def two_frame_OF(frame_1, frame_2, window_size, smooth_window_size, derivative_mask_size):
    
    "smooth images with gaussian"
    frame_1_smooth = cv.GaussianBlur(frame_1, smooth_window_size, 0)
    frame_2_smooth = cv.GaussianBlur(frame_2, smooth_window_size, 0)
    
    "calculate x, y and t derivatives"
    frame_1_x_derivative = cv.Sobel(frame_1_smooth, cv.CV_64F, 1, 0, ksize=derivative_mask_size)
    frame_1_x_derivative = -frame_1_x_derivative
    frame_1_y_derivative = cv.Sobel(frame_1_smooth, cv.CV_64F, 0, 1, ksize=derivative_mask_size)
    
    temp = frame_1_x_derivative
    frame_1_x_derivative = frame_1_y_derivative
    frame_1_y_derivative = temp
    
    frames_t_derivative = frame_2_smooth - frame_1_smooth
    
    "This list will contain magnitures of flow vectors in u and v direction"
    motion_vectors = []
    
    "use harris corner detector for finding points in which OF is calculated"
    hc = cv.cornerHarris(frame_1_smooth, 2, 3, 0.04) 
    xy = np.argwhere(hc>0.01*hc.max())
    
    "use grid for finding points in which OF is calculated"
    "create meshgrid of pixel position for optical flow calculation"
    #n_grid_points = 20
    #x = range(window_size, frame_1.shape[0]-window_size, n_grid_points)
    #y = range(window_size, frame_1.shape[1]-window_size, n_grid_points)
    #xv, yv = np.meshgrid(x, y)
    #xy = np.array(list(zip(xv.ravel(), yv.ravel())))
    
    "for every position calculate OF"
    for pos in xy:
        
        i = pos[0]
        j = pos[1]
           
        "take pixels around grid point"
        Ix = frame_1_x_derivative[i-window_size:i+window_size+1, j-window_size:j+window_size+1].flatten()
        Iy = frame_1_y_derivative[i-window_size:i+window_size+1, j-window_size:j+window_size+1].flatten()
        It = frames_t_derivative[i-window_size:i+window_size+1, j-window_size:j+window_size+1].flatten()
        
        "Original M matrix"
        M = np.array([[np.sum(np.power(Ix,2)), np.sum(Ix*Iy)], [np.sum(Ix*Iy), np.sum(np.power(Iy,2))]])
        
        "Original b vector"
        b = -np.array([[np.sum(Ix*It)],[np.sum(Iy*It)]])
     
        
        "for motion vecotrs, calculate pseudoinverse"
        motion_vector = np.dot(np.linalg.pinv(M).T, b)
        motion_vectors.append(motion_vector)
    
    return motion_vectors, xy